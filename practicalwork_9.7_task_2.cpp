﻿
#include <iostream>
#include <vector>
#include <thread>
#include <algorithm>
#include <mutex>
#include <chrono>
#include <conio.h>
#include <ctime>
#include <string>
#include <stdio.h>
#include <Windows.h>
#include <stdlib.h>
#include <condition_variable>
#include <random>

using namespace std;

condition_variable CV_FillVector;
int nScreenWidth = 120;
int nScreenHeight = 30;
bool GameContinues = true;
int nFieldWidth = 12;
int nFieldHeight = 18;
int nCurrentX = 0;
int LivesCount = 3;
int CurrentPawnPosition = (nFieldHeight - 1) * nScreenWidth + 7;

int GetRandomInRange(const int min, const int max)
{
    static default_random_engine gen(
        static_cast<unsigned>(
            chrono::system_clock::now().time_since_epoch().count())
    );
    uniform_int_distribution<int> destribution(min, max);
    return destribution(gen);
}

void GettingPlayerInput(mutex& locker, int& currentX)
{
    this_thread::sleep_for(chrono::milliseconds(175));
    unique_lock<mutex> UL(locker, defer_lock);

    bool bKey[4];

    while (GameContinues)
    {
        for (int k = 0; k < 2; k++)
            bKey[k] = (0x8000 & GetAsyncKeyState((unsigned char)("\x27\x25"[k]))) != 0;

        if (bKey[0])
        {
            UL.lock();
            nCurrentX = 1;
            UL.unlock();
        }

        if (bKey[1])
        {
            UL.lock();
            nCurrentX = - 1;
            UL.unlock();
        }
    }
}

void RenderGraphics(mutex& locker, int& nScreenWidth, int& nScreenHeight, vector<char>& ObstaclesArray, vector<int>& ObstaclesPosition)
{
    unique_lock<mutex> UL(locker, defer_lock);
    this_thread::sleep_for(chrono::milliseconds(150));

    int AppearanceCounter = 0;

    wchar_t* screen = new wchar_t[nScreenWidth * nScreenHeight];
    for (int i = 0; i < nScreenWidth * nScreenHeight; i++) screen[i] = L' ';
    HANDLE hConsole = CreateConsoleScreenBuffer(GENERIC_READ | GENERIC_WRITE, 0, 
        NULL, CONSOLE_TEXTMODE_BUFFER, NULL);
    SetConsoleActiveScreenBuffer(hConsole);
    DWORD dwBytesWritten = 0;

    unsigned char* pField = nullptr;

    pField = new unsigned char[nFieldWidth * nFieldHeight];
    for (int x = 0; x < nFieldWidth; x++)
        for (int y = 0; y < nFieldHeight; y++)
            pField[y * nFieldWidth + x] = (x == 0 || x == nFieldWidth - 1 || y == nFieldHeight - 1) ? 9 : 0;

    while (GameContinues)
    {
        for (int x = 0; x < nFieldWidth; x++)
            for (int y = 0; y < nFieldHeight; y++)
                screen[(y + 2) * nScreenWidth + (x + 2)] = L" ABCDEFG=#"[pField[y * nFieldWidth + x]];

        if (CurrentPawnPosition >= (nFieldHeight - 1) * nScreenWidth + 3 &&
            CurrentPawnPosition <= (nFieldHeight - 1) * nScreenWidth + (nFieldWidth))
        {
            UL.lock();
            CurrentPawnPosition += nCurrentX;
            nCurrentX = 0;
            UL.unlock();
        }
        else
        {
            if (CurrentPawnPosition < (nFieldHeight - 1) * nScreenWidth + 3)
            {
                CurrentPawnPosition += 2;
            }
            if (CurrentPawnPosition > (nFieldHeight - 1) * nScreenWidth + (nFieldWidth))
            {
                CurrentPawnPosition -= 2;
            }
        }

        AppearanceCounter++;
        if (AppearanceCounter >= 2)
        {
            if (ObstaclesArray.size() < 10)
            {
                ObstaclesArray.push_back(L" BCDEFG=#"[GetRandomInRange(0, 8)]);
                ObstaclesPosition.push_back(nScreenWidth * 2 + GetRandomInRange(3, nFieldWidth));
            }

            for (int i = 0; i <= ObstaclesArray.size()-1; i++)
            {
                if(ObstaclesArray.at(i))
                {
                    screen[ObstaclesPosition.at(i)] = ObstaclesArray.at(i);
                    ObstaclesPosition.at(i) = ObstaclesPosition.at(i) + nScreenWidth;
                }
            }

            AppearanceCounter = 0;
        }

        screen[CurrentPawnPosition] = L" ABCDEFG=#"[1];

        WriteConsoleOutputCharacter(hConsole, screen, nScreenWidth * nScreenHeight, { 0,0 }, &dwBytesWritten);
        this_thread::sleep_for(chrono::milliseconds(350));
    }

    CloseHandle(hConsole);
}

void HandlingObstaclesAndCollisions(mutex& locker, vector<char>& ObstaclesArray, vector<int>& ObstaclesPosition)
{
    unique_lock<mutex> UL(locker, defer_lock);

    while (GameContinues)
    {
        this_thread::sleep_for(chrono::milliseconds(100));

        if (!ObstaclesPosition.empty())
        {
            for (int i = 0; i < ObstaclesPosition.size(); i++)
        {
            if (CurrentPawnPosition == ObstaclesPosition.at(i))
            {
                LivesCount--;
                this_thread::sleep_for(chrono::milliseconds(550));
                if (LivesCount <= 0) GameContinues = false;
            }

            if (ObstaclesPosition.at(i) > (nFieldHeight - 1) * nScreenWidth + nFieldWidth)
            {
                UL.lock();
                ObstaclesArray.erase(ObstaclesArray.begin() + i);
                ObstaclesPosition.erase(ObstaclesPosition.begin() + i);
                UL.unlock();
            }
        }
        }
    }
}

int main()
{
    mutex m;
    mutex& lockerForMyVector = m;
    vector<char> ObstaclesArray;
    vector<int> ObstaclesPosition;

    thread GettingPlayerInput_thread(GettingPlayerInput, ref(lockerForMyVector), ref(nCurrentX));
    thread RenderGraphics_thread(RenderGraphics, ref(lockerForMyVector), ref(nScreenWidth), ref(nScreenHeight), ref(ObstaclesArray), ref(ObstaclesPosition));
    thread HandlingObstaclesAndCollisions_thread(HandlingObstaclesAndCollisions, ref(lockerForMyVector), ref(ObstaclesArray), ref(ObstaclesPosition));

    GettingPlayerInput_thread.join();
    RenderGraphics_thread.join();
    HandlingObstaclesAndCollisions_thread.join();

    cout << "Game Over!" << endl;
    system("pause");
}
